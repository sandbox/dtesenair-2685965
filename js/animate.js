jQuery(function($) {

  //Initialize the Superfish main menu
  $(document).ready(function() {

    var animateUp = ', .lay-director-highlight-block .ld-photo img';
    var animateDown = '.lay-director-highlight-block .block-title';
    var animateLeft = '.front .upcoming-events-highlight-block .title-content';
        animateLeft += ',.front .latest-articles-highlight-block .title-content';
        animateLeft += ', #block-views-walk_countdown-block .left';
        animateLeft += ', .lay-director-highlight-block .content';
    var animateRight = '#block-views-walk_countdown-block .right';
    
    $(animateUp).addClass("animated");
    $(animateDown).addClass("animated");
    $(animateLeft).addClass("animated");
    $(animateRight).addClass("animated");


    //ADD CLASS TO "ANIMATE" ELEMENTS
    $(window).scroll( function(){    
      
        //fadeInLeft      
        $(animateLeft).each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("animated fadeInLeft");
            }
        }); 
        ///fadeInRight
        $(animateRight).each( function(i){
            $(this).addClass("animate");
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("animated fadeInRight");
            }
        }); 

        //fadeInUp
        $(animateUp).each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("fadeInUp");
            }
        }); 

        //fadeInDown
        $(animateDown).each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("fadeInDown");
            }
        }); 

        
    }); //window.scroll
 


  }); //end $(document).ready

});
