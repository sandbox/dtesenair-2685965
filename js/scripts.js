jQuery(function($) {


  $(window).load(function() {

      //this script adds a class to every image specifying wether the image is landscape or portrait
      $('body').find('img').each(function() {
          var imgClass = (this.width / this.height > 1) ? 'landscape' : 'portrait';
          $(this).addClass(imgClass);
      })

  }) //

  //Initialize the Superfish main menu
  $(document).ready(function() {




        $('#map').addClass('scrolloff');                // set the mouse events to none when doc is ready
        
        $('#overlay').on("mouseup",function(){          // lock it when mouse up
            $('#map').addClass('scrolloff'); 
            //somehow the mouseup event doesn't get call...
        });
        $('#overlay').on("mousedown",function(){        // when mouse down, set the mouse events free
            $('#map').removeClass('scrolloff');
        });

        $("#map").mouseleave(function () {              // becuase the mouse up doesn't work... 
            $('#map').addClass('scrolloff');            // set the pointer events to none when mouse leaves the map area
                                                        // or you can do it on some other event
        });
        








    //SUPERFISH INITIALIZATION
    //$('ul.sf-menu').superfish({
    //$('#menu-wrap .menu, #block-system-navigation .menu').superfish({
    $('#menu-wrap .menu').superfish({
      delay : 500, // one second delay on mouseout
      animation : {
        opacity : 'show',
        height : 'show'
      }, // fade-in and slide-down animation
      speed : 'fast', // faster animation speed
      autoArrows : true, // disable generation of arrow mark-up
      dropShadows : false // disable drop shadows
    });

    //SCROLL-TO-TOP FUNCTIONS
    $(window).scroll(function() {
      if ($(this).scrollTop() > 100) {
        $('#scroll-to-top').fadeIn();
      } else {
        $('#scroll-to-top').fadeOut();
      }
    });

    $('#scroll-to-top').click(function() {
      $("html, body").animate({
        scrollTop : 0
      }, 1000);
      return false;
    });
    //end SCROLL-TO-TOP



    //Adds class to #site-header when the document is scrolled down
    $(window).on("scroll", function() {
      //if ($(this).scrollTop() > 150) {
      if ($(this).scrollTop() > 0) {
        $("#header-wrap").addClass("scrolled");
      } else {
        $("#header-wrap").removeClass("scrolled");
      }
    });

    //Animates in-page anchor links by smoothly scrolling the page
    $(function() {
      $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            $('html,body').animate({
              scrollTop : target.offset().top
            }, 500);
            return false;
          }
        }
      });
    });

  }); //end $(document).ready

});
