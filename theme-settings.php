<?php


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function tesenair_form_system_theme_settings_alter(&$form, &$form_state) {

  //drupal_set_message('<pre>' . print_r($form, TRUE) . '</pre>');
  //drupal_set_message('<pre>' . theme_get_setting('default_alternate_logo') . '</pre>');
  
  $form['tesenair_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tesenair Theme Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['tesenair_settings']['clear_registry'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Rebuild theme registry on every page.'),
    '#description'   =>t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
    '#default_value' => theme_get_setting('clear_registry'),
  );
  $form['tesenair_settings']['scroll_to_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show scroll to top arrow'),
    '#default_value' => theme_get_setting('scroll_to_top','tesenair'),
    '#description'   => t("Check this option to enable the Scroll to Top javascript. Uncheck to hide."),
  );
  $form['tesenair_settings']['breadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show breadcrumbs in a page'),
    '#default_value' => theme_get_setting('breadcrumbs','tesenair'),
    '#description'   => t("Check this option to show breadcrumbs in page. Uncheck to hide."),
  );
  
  
  
  $form['tesenair_settings']['socialicon'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Icons'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tesenair_settings']['socialicon']['display_social_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Social Icons'),
    '#default_value' => theme_get_setting('display_social_icons','tesenair'),
    '#description'   => t("Select this option to display social media icons"),
  );
  $form['tesenair_settings']['socialicon']['twitter_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter Profile'),
    '#default_value' => theme_get_setting('twitter_url', 'tesenair'),
    '#description'   => t("Enter your Twitter Profile URL. Leave blank to hide."),
  );
  $form['tesenair_settings']['socialicon']['facebook_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Profile'),
    '#default_value' => theme_get_setting('facebook_url', 'tesenair'),
    '#description'   => t("Enter your Facebook Profile URL. Leave blank to hide."),
  );
  $form['tesenair_settings']['socialicon']['google_plus_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Google+ Profile'),
    '#default_value' => theme_get_setting('google_plus_url', 'tesenair'),
    '#description'   => t("Enter your Google Plus URL. Leave blank to hide."),
  );
  $form['tesenair_settings']['socialicon']['pinterest_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Pinterest'),
    '#default_value' => theme_get_setting('pinterest_url', 'tesenair'),
    '#description'   => t("Enter your Pinterest URL. Leave blank to hide."),
  );
}
