<?php
/* Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they can be found in the
 * html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * $base_path: The base URL path of the Drupal installation. At the very least, this will always default to /.
 * $directory: The directory the template is located in, e.g. modules/system or themes/bartik.
 * $is_front: TRUE if the current page is the front page.
 * $logged_in: TRUE if the user is registered and signed in.
 * $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * $front_page: The URL of the front page. Use this instead of $base_path, when linking to the front page. This includes the language domain or prefix.
 * $logo: The path to the logo image, as defined in theme configuration.
 * $site_name: The name of the site, empty when display has been disabled in theme settings.
 * $site_slogan: The slogan of the site, empty when display has been disabled in theme settings.
 *
 * Navigation:
 * $main_menu (array): An array containing the Main menu links for the site, if they have been configured.
 * $secondary_menu (array): An array containing the Secondary menu links for the site, if they have been configured.
 * $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * $title_prefix (array): An array containing additional output populated by modules, intended to be displayed in front of the main title tag that appears in the template.
 * $title: The page title, for use in the actual HTML content.
 * $title_suffix (array): An array containing additional output populated by modules, intended to be displayed after the main title tag that appears in the template.
 * $messages: HTML for status and error messages. Should be displayed prominently.
 * $tabs (array): Tabs linking to any sub-pages beneath the current page (e.g., the view and edit tabs when displaying a node).
 * $action_links (array): Actions local to the page, such as 'Add menu' on the menu administration interface.
 * $feed_icons: A string of all feed icons for the current page.
 * $node: The node object, if there is an automatically-loaded node associated with the page, and the node ID is the second argument in the page's path (e.g. node/12345 and node/12345/revisions, but not comment/reply/12345).
 *
 * Regions:
 * $page['help']: Dynamic help text, mostly for admin pages.
 * $page['highlighted']: Items for the highlighted content region.
 * $page['content']: The main content of the current page.
 * $page['sidebar_first']: Items for the first sidebar.
 * $page['sidebar_second']: Items for the second sidebar.
 * $page['header']: Items for the header region.
 * $page['footer']: Items for the footer region.
 */
?>



<header id="header-wrap">

  <div id="header-left">
		<div id="logo">
			<?php if ($logo): ?>
			  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
				<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
			  </a>
			<?php endif; ?>
			
			<?php if (theme_get_setting('tesenair_default_logo','tesenair')==1 ): ?>
				<div id="alt-logo">
				  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
					<img src="<?php theme_get_setting('tesenair_default_logo', 'tesenair'); ?>" alt="<?php print t('Home'); ?>" />
					<img src="logo.png" alt="<?php print t('Home'); ?>" />
				  </a>
				</div>
		  <?php endif; ?>	  
		</div>

		<div id="site-name">	
			<?php if ($site_name): ?>
				<h1><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
			<?php endif; ?>
		</div>
	
		<div id="site-slogan">
			<?php if ($site_slogan): ?>
				<h2><?php print $site_slogan; ?></h2>
			<?php endif; ?>
		</div>
	</div>
	
	<div id="header-right">
		<?php if (theme_get_setting('display_social_icons', 'tesenair')): ?>
			<?php
      $twitter_url = check_plain(theme_get_setting('twitter_url'));
      $facebook_url = check_plain(theme_get_setting('facebook_url'));
      $google_plus_url = check_plain(theme_get_setting('google_plus_url'));
      $pinterest_url = check_plain(theme_get_setting('pinterest_url'));
			?>
			<div id="social-icons">
				<ul>
					<?php if ($facebook_url): ?>
						<li><a target="_blank" title="<?php print $site_name; ?> on Facebook" href="<?php print $facebook_url; ?>">
								<i class="fa fa-facebook"></i> 
							</a></li>
            <?php endif; ?>
            <?php if ($twitter_url): ?>
              <li><a target="_blank" title="<?php print $site_name; ?> on Twitter" href="<?php print $twitter_url; ?>">
                  <i class="fa fa-twitter"></i> 
                </a></li>
            <?php endif; ?>
            <?php if ($google_plus_url): ?>
              <li><a target="_blank" title="<?php print $site_name; ?> on Google+" href="<?php print $google_plus_url; ?>">
                  <i class="fa fa-google-plus"></i>
                </a></li>
            <?php endif; ?>
            <?php if ($pinterest_url): ?>
              <li><a target="_blank" title="<?php print $site_name; ?> on Pinterest" href="<?php print $pinterest_url; ?>">
                  <i class="fa fa-pinterest-p"></i> 
                </a></li>
            <?php endif; ?>
            <li><a target="_blank" title="<?php print $site_name; ?> via RSS" href="<?php print $front_page; ?>rss.xml">
                <i class="fa fa-rss"></i>
              </a></li>
          </ul>
        </div>
		
      <?php endif; ?>

	    	  
	</div>
 	
	<div id="main-menu" >
		<?php if ($main_menu): ?>
		  <?php //print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
      <?php
      $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
      print drupal_render($main_menu_tree);
        ?>
		<?php endif; ?>
	</div>

</header>




  <?php if ($page['highlight']): ?>
   <!-- <div id="bottom-highlight1-wrap" class="parallax-window" data-positionY="-25px" data-parallax="scroll" data-speed="0.3" data-image-src="/<?php print drupal_get_path('theme', $GLOBALS['theme']); ?>/images/bottom-highlight1-bg.jpg"> --></div>
    <div id="highlight-wrap">
      <?php print render($page['highlight']) ?>
    </div>
<?php endif; ?>



  <section id="content-wrap">

      <div id="title-wrap">
        <?php print render($title_prefix); ?>
        
        <?php if ($title): ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?>
      
        <?php print render($title_suffix); ?>
      
      </div>

    <div id="content">


    <?php if (theme_get_setting('breadcrumbs') == 1 && !$is_front) print $breadcrumb; ?>

    <?php print $messages; ?>
    <?php print render($page['help']); ?>

  	<?php if ($tabs): ?>
  	  <?php print render($tabs); ?>
  	<?php endif; ?>
  	
  	<?php if ($action_links): ?>
  	  <ul><?php print render($action_links); ?></ul>
  	<?php endif; ?>
  	
  	<?php print render($page['content']) ?>
  </div>


</section>


<?php if ($page['bottom_highlight1']): ?>
   <!-- <div id="bottom-highlight1-wrap" class="parallax-window" data-positionY="-25px" data-parallax="scroll" data-speed="0.3" data-image-src="/<?php print drupal_get_path('theme', $GLOBALS['theme']); ?>/images/bottom-highlight1-bg.jpg"> --></div>
    <div id="bottom-highlight1-wrap">
      <?php print render($page['bottom_highlight1']) ?>
    </div>
<?php endif; ?>
 
 
<?php if ($page['bottom_highlight2']): ?>
    <div id="bottom-highlight2-wrap">
      <?php print render($page['bottom_highlight2']) ?>
    </div>
<?php endif; ?>


<footer id="footer-wrap">
  <?php if ($page['footer']) print render($page['footer']); ?>
  <?php if ($page['copyright']) print render($page['copyright']); ?>
</footer>

